#!/usr/bin/env node
'use strict';

const rp = require('request-promise');
const program = require('commander');
const path = require('path');
const request = require('request');
const fs = require('fs');
const async = require('async');

program
  .version('1.0.0')
  .option('-n, --nb [number]', 'Number of maximum parallise request', parseInt)
  .option('-f, --file <path>', 'Path to json file containing urls')
  .parse(process.argv);

if (!program.file){
  throw new Error('file path is required')
  process.exit(1);
}

// Read the json file sent
let urls = require(program.file)
let fileToWrite = path.resolve(program.file);
let results = {};

// Request all id from the object
async.eachOfLimit(urls, program.nb || 30, function (url, key, next) {
  request(url, function(error,res,body){
    if(error && res.statusCode !== 200 ){
      console.error(`Error ${res.statusCode} @ ${url}`)
      return next();
    }
    body = JSON.parse(body) // parse string to object
    results[key] = body.paths;
    next();
  })
}, function (err) {
  if (err) console.error(err.message);
  results = JSON.stringify(results);
  fs.writeFileSync(fileToWrite, results, {encoding: 'utf8'})
});